package com.music;

public class Main {
  public static void main(String[] args)
  {
    Cathalog cathalog = new Cathalog("My strange playlist");
    Artist eliseRyd = new Artist("Elise Ryd");
    Group amaranthe  = new Group("Amaranthe");
    //Genre rock = new Genre("Rock");
    //List<Genre> parents = new ArrayList<>();
    //parents.add(rock);
    //Genre modernMetal = new Genre("Modern Metal", parents);
    Album nexusAlbum = new Album(amaranthe,"The Nexus", new HeavyRockGenre(), 2017);
    Song nexusSong = new Song("The Nexus", nexusAlbum);
    cathalog.addAlbum(nexusAlbum);
    cathalog.addArtist(eliseRyd);
    cathalog.addGroup(amaranthe);
    Compilation comp = new Compilation("My compilation");
    comp.addAlbum(nexusAlbum);
    cathalog.addCompilation(comp);
    BaseGenre genre = new HeavyRockGenre();
    //cathalog.addGenre(rock);
    //cathalog.addGenre(modernMetal);
    System.out.println(cathalog.getAlbums("Heavy Rock", 2017));
    System.out.println(cathalog.getCompilations("Rock", 2017));
    System.out.println(cathalog.getSongs("Rock", 2017));
  }
}
