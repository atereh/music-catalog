# Music catalog

This project provides solving an academic task: implement a music Catalog using OOP principles and Java.

### Details of architecture:

Some **Artists** release **Albums** that contain **Songs** (or tracks).
There are also **musical genres**. Genres have **subgenres** (for example, Heavy rock is a subgenre of Rock)
In addition to albums, the catalog contains **Compilations** - the compilations contain tracks by different artists, possibly of different genres.

### Task:
Design and implement a class model of such a Directory, and make it a small search engine. 
For example, to find all albums and compilations that contain tracks of the Rock genre released in 2017 (there should also be albums of the Heavy rock genre, as a subgenre of Rock). 
The engine should be able to search for a) artists, b) albums and compilations, C) songs by several criteria (define the criteria yourself)

### Some clarifications on realisation:

- You can set a specific genre (or genres) for an Artist and assume that all of their tracks are made in that genre. Or consider that each track can have its genre (or album, or something else)
- We believe that artists can't release an album together (one album - by one artist)
- You can assume that the track does not exist outside of the album. So if the track is released, that means, it must be present in at least one album, even if this track is the only one there (such albums are called Singles)
- Genres can be represented as a tree (in this case, each subgenre can have only one ancestor). But you can also implement other relationships (for example, assume that the Pop-rock genre has the parent Rock and Pop genres). 
  Anyway, you need to implement at least two levels of genres (that is: the basic parent genre and its subgenres)
- We can assume that the Collection is an arbitrary set of tracks from previously released albums (different artists)

### Contributors:
Alina Terekhova
