package com.music;

import java.util.Iterator;
import java.util.LinkedList;

public class Album implements Iterable<Song> {
  private Group author;
  private LinkedList<Song> songs;
  private String name;
  private BaseGenre genre;
  private Integer year;



  public Album(Group author, String name, BaseGenre genre, Integer year)
  {
    this.songs = new LinkedList<Song>();
    this.author=author;
    this.name = name;
    this.genre = genre;
    this.year = year;
    author.addAlbum(this);
  }

  public void addSong(Song song)
  {
    this.songs.add(song);
  }

  public BaseGenre getGenre() {
    return genre;
  }

    public Integer getYear() {
        return year;
    }

  public Group getAuthor() {
    return author;
  }

  public String getName() {
    return name;
  }

  @Override
  public Iterator<Song> iterator() {
    return songs.iterator();
  }

  @Override
  public String toString() {
    return author.toString() + "'s album " + name;
  }
}
