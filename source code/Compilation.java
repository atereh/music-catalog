package com.music;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Compilation implements Iterable<Album> {
  private String name;
  private List<Album> albums;

  public Compilation(String name)
  {
    this.name=name;
    this.albums=new LinkedList<>();
  }

  public String getName() {
    return name;
  }

  public List<Album> getAlbums() {
    return albums;
  }

  public void setAlbumsList(List<Album> albums) {
    this.albums = albums;
  }

  public void addAlbum(Album album)
  {
    albums.add(album);
  }


    @Override
    public Iterator<Album> iterator() {
        return albums.iterator();
    }

  @Override
  public String toString() {
    return name;
  }
}
