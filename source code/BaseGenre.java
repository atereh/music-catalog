package com.music;

import java.util.ArrayList;

public class BaseGenre {

    protected ArrayList<String> tree;
    protected String name;

    public BaseGenre()
    {
        this.tree = new ArrayList<>();
        this.name="Genre";
        this.tree.add(this.name);
    }

    public String getName() {
        return name;
    }

    public boolean isOf(String genrename){
        return this.tree.contains(genrename);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null || this.getClass()!=obj.getClass())
        {
            return false;
        }
        return this.name == ((BaseGenre)obj).name;
    }
}
