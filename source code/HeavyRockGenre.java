package com.music;

public class HeavyRockGenre extends RockGenre{


    public HeavyRockGenre()
    {
        this.name="Heavy Rock";
        this.tree.add(this.name);
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null || this.getClass()!=obj.getClass())
        {
            return false;
        }
        return this.name == ((BaseGenre)obj).name;
    }
}
